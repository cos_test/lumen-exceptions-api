<?php

namespace App\Http\Controllers;

use App\Models\Exception;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ExceptionController extends Controller
{
    use ApiResponse;

    /**
     * Return exceptions list
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $exceptions = Exception::all();
        return $this->successResponse($exceptions);
    }

    /**
     * Create an instance of Exception
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $rules = [
            'code' => 'numeric',
        ];

        $this->validate($request, $rules);
        $exception = Exception::create($request->all());
        return $this->successResponse($exception, ResponseAlias::HTTP_CREATED);
    }

    /**
     * Return an specific exception
     * @param $exception
     * @return JsonResponse
     */
    public function show($exception): JsonResponse
    {
        $exception = Exception::findOrFail($exception);
        return $this->successResponse($exception);
    }

    /**
     * Update thej information of an existing exception
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $exception): JsonResponse
    {
        $rules = [
            'code' => 'numeric',
        ];

        $this->validate($request, $rules);
        $exception = Exception::findOrFail($exception);
        $exception->fill($request->all());

        if ($exception->isClean()) {
            return $this->errorResponse('At least one value must change', ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }

        $exception->save();
        return $this->successResponse($exception);
    }

    /**
     * Removes an existing exception
     * @param $exception
     * @return JsonResponse
     */
    public function destroy($exception): JsonResponse
    {
        $exception = Exception::findOrFail($exception);
        $exception->delete();
        return $this->successResponse($exception);
    }
}
