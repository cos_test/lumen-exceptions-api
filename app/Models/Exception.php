<?php

namespace App\Models;

class Exception extends \Illuminate\Database\Eloquent\Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'error',
        'code',
    ];
}
