<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/exceptions', 'ExceptionController@index');
$router->post('/exceptions', 'ExceptionController@store');
$router->get('/exceptions/{exception}', 'ExceptionController@show');
$router->put('/exceptions/{exception}', 'ExceptionController@update');
$router->patch('/exceptions/{exception}', 'ExceptionController@update');
$router->delete('/exceptions/{exception}', 'ExceptionController@destroy');
